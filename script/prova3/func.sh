#!/bin/bash

# Função para exibir o menu de configurações
exibir_menu() {
    clear
    echo "Menu de Customização PS1"
    echo "1. Mudar Cor"
    echo "2. Exibir Apenas o Nome do Usuário"
    echo "3. Prompt em Duas Linhas"
    echo "4. Personalização Extra"
    echo "5. Restaurar para Padrão"
    echo "6. Sair"
}

mudar_cor() {
    declare -A cores=(
        ["preto"]="30"
        ["vermelho"]="31"
        ["verde"]="32"
        ["amarelo"]="33"
        ["azul"]="34"
        ["magenta"]="35"
        ["ciano"]="36"
        ["branco"]="37"
    )

    read -p "Digite o nome da cor desejada: " nome_cor

    codigo_cor=${cores["$nome_cor"]}

    if [ -n "$codigo_cor" ]; then
        export PS1="\[\033[01;${codigo_cor}m\]$PS1\[\033[00m\]"
        echo "Cor do Prompt modificada para $nome_cor."
        sleep 2
    else
        echo "Cor não reconhecida. Nenhuma alteração feita."
        sleep 2
    fi
}

# Função para exibir apenas o nome do usuário
exibir_apenas_nome_usuario() {
    export PS1='\u\$ '
    echo "Prompt configurado para exibir apenas o nome do usuário."
    sleep 2
}

# Função para configurar o prompt em duas linhas
prompt_em_duas_linhas() {
    export PS1='\u@\h \n\$ '
    echo "Prompt configurado para duas linhas."
    sleep 2
}

# Função para personalização extra
personalizacao_extra() {
    # Personalize conforme necessário
    read -p "Digite sua personalização extra para o PS1: " personalizacao_extra
    export PS1="$personalizacao_extra"
    echo "Prompt personalizado: $PS1"
    sleep 2
}

# Função para restaurar para o prompt padrão
restaurar_padrao() {
    export PS1='\u@\h \W\$ '
    echo "Prompt restaurado para o padrão."
    sleep 2
}

