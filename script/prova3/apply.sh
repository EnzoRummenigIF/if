#!/bin/bash

source func.sh

exibir_menu
read -p "Digite a opção desejada: " opcao

case $opcao in
    1)
        mudar_cor
        ;;
    2)
        exibir_apenas_nome_usuario
        ;;
    3)
        prompt_em_duas_linhas
        ;;
    4)
        personalizacao_extra
        ;;
    *)
        ;;
esac

