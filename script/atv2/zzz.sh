#!/bin/bash

arquivo=""

while true; do
  echo "Selecione uma opção: "
  echo "1. Usar um arquivo ZIP pré-existente"
  echo "2. Criar um novo arquivo ZIP"
  echo "3. Sair"

  read -p "Escolha uma opção (1-3): " escolha
  sleep 1

  if [ "$escolha" -eq 1 ]; then
    read -p "Digite o nome do arquivo ZIP existente: " arquivo
    if [ -f "$arquivo" ]; then
      break
    else
      echo "O arquivo ZIP especificado não existe."
    fi
  elif [ "$escolha" -eq 2 ]; then
    read -p "Digite o nome do novo arquivo ZIP a ser criado: " arquivo  
    echo -n | zip "$arquivo" -
    break
  elif [ "$escolha" -eq 3 ]; then
    exit
  else
    echo "Opção inválida. Escolha um número de 1 a 3."
  fi
done

while true; do
  echo "Selecione uma opção:"
  echo "1. Listar o conteúdo do arquivo ZIP"
  echo "2. Pré-visualizar um arquivo no ZIP"
  echo "3. Adicionar arquivos ao ZIP"
  echo "4. Remover Arquivos do ZIP"
  echo "5. Extrair todo o conteúdo do ZIP"
  echo "6. Extrair arquivos específicos do ZIP"
  echo "7. Sair"

  read -p "Escolha uma opção (1-7): " escolha_acao
  sleep 1
  if [ "$escolha_acao" -eq 1 ]; then
    unzip -l "$arquivo"
  elif [ "$escolha_acao" -eq 2 ]; then
    read -p "Nome do arquivo a pré-visualizar: " arquivo_previsualizar
    unzip -p "$arquivo" "$arquivo_previsualizar"
  elif [ "$escolha_acao" -eq 3 ]; then
    read -p "Nome do arquivo a adicionar: " arquivo_adicionar
    zip -u "$arquivo" "$arquivo_adicionar"
  elif [ "$escolha_acao" -eq 4 ]; then
    read -p "Nome do arquivo a remover: " arquivo_remover
    zip -d "$arquivo" "$arquivo_remover"
  elif [ "$escolha_acao" -eq 5 ]; then
    read -p "Diretório para extrair todo o conteúdo: " diretorio_extracao
    unzip -q "$arquivo" -d "$diretorio_extracao"
  elif [ "$escolha_acao" -eq 6 ]; then
    read -p "Nome do arquivo a extrair: " arquivo_extrair
    read -p "Diretório para extrair o arquivo específico: " diretorio_extracao_especifico
    unzip -q "$arquivo" "$arquivo_extrair" -d "$diretorio_extracao_especifico"
  elif [ "$escolha_acao" -eq 7 ]; then
    exit
  else
    echo "Opção inválida. Escolha um número de 1 a 7."
  fi
done




