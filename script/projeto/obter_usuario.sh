#!/bin/bash

source erro.sh

obter_usuario() {
    local usuario
    usuario=$(yad --form --title "Usando SCP 2/3 " \
        --text "Agora informe o usuário da máquina:" \
        --field "Usuário ->:text" )

    [ $? -eq 0 ] || exibir_mensagem_erro "Processo encerrado pelo usuário."

    [ -n "$usuario" ] || exibir_mensagem_erro "Usuário não fornecido."

    echo "$usuario"
}
