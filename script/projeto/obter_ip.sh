#!/bin/bash

source erro.sh

obter_ip() {
    local ip
    ip=$(yad --form --title "Usando SCP 3/3" \
        --text "Por último, informe o IP da máquina:" \
        --field "IP ->:text" )

    [ $? -eq 0 ] || exibir_mensagem_erro "Processo encerrado pelo usuário."

    [ -n "$ip" ] || exibir_mensagem_erro "Endereço IP não fornecido."

    echo "$ip"
}
