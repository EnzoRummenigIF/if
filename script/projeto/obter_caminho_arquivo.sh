#!/bin/bash

source erro.sh

obter_caminho_arquivo() {
    local arquivo
    arquivo=$(yad --file --title "Usando SCP 1/3" \
        --text "Primeiro informe o arquivo:" \
        --field "Arquivo ->:FL")

    [ $? -eq 0 ] || exibir_mensagem_erro "Processo encerrado pelo usuário."

    [ -n "$arquivo" ] || exibir_mensagem_erro "Caminho do arquivo não fornecido."

    echo "$arquivo"
}
