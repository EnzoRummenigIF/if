#!/bin/bash

source erro.sh

realizar_copia() {
    local arquivo="$1"
    local usuario="$2"
    local ip="$3"

    scp "$arquivo" "$usuario@$ip:/home/$usuario"

    if [ $? -eq 0 ]; then
        echo "Cópia realizada com sucesso."
    else
        exibir_mensagem_erro "Erro ao copiar o arquivo."
    fi
}
