#!/bin/bash

source obter_caminho_arquivo.sh
source obter_usuario.sh
source obter_ip.sh
source realizar_copia.sh

executar_script() {
    local arquivo
    local usuario
    local ip

    arquivo=$(obter_caminho_arquivo)
    usuario=$(obter_usuario)
    ip=$(obter_ip)

    realizar_copia "$arquivo" "$usuario" "$ip"
}

executar_script
